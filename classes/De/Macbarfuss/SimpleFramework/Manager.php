<?php
namespace De\Macbarfuss\SimpleFramework;

use De\Macbarfuss\SimpleFramework\Builder\NavBuilder;
use De\Macbarfuss\SimpleFramework\Builder\QuotationMarkBuilder;
use De\Macbarfuss\SimpleFramework\Builder\SnippetHandler;

/**
 * like a main class for the framework
 * 
 */
class Manager
{

    public $navBuilder;

    public $pageBuilder;

    public $featureconfig;

    public $features;

    public $site;

    public $style;

    /**
     * default constructor
     * 
     * @param array $site part of the global configuration
     * @param array $featureconfig part of the global configuration
     * @param array $style part of the global configuration
     */
    public function __construct($site, $featureconfig, $style = array('nav' => array()))
    {
        // init co managers
        $this->pageBuilder = new PageBuilder();
        ClassRegistry::addInstance("PageBuilder", $this->pageBuilder);

        $this->featureconfig = $featureconfig;
        $this->site = $site;
        $this->style = $style;

        ClassRegistry::addInstance("NavBuilder", new NavBuilder($site, $style['nav']));
        ClassRegistry::addInstance("QuotationMarkBuilder", new QuotationMarkBuilder($style));
        ClassRegistry::addInstance("SnippetHandler", new SnippetHandler($site));
        $this->features = $this->getActiveFeatures();
    }

    /**
     * getter for the site config part given to the manager
     * 
     * @return array site configuration
     */
    public function getSiteConfig()
    {
        return $this->site;
    }

    /**
     * getter for a link to a site defined in the site structure
     * @param string $targetId name of the site in the structure
     * @param string $text optional different linktext to that defined in the site structure
     * @param array $attributes optional array wit attributes for the link
     * @return string the DOM
     */
    public function getInternalLink($targetId, $text = '', $attributes = array())
    {
        $data = $this->getSiteStructureItemData($targetId);
        if ($data !== null) {
            $path = $this->getStructureItemPath($targetId);
            $pathstring = '';
            $pathfirst = true;
            foreach ($path as $folder) {
                if ($pathfirst) {
                    $pathfirst = false;
                } else {
                    $pathstring .= '/';
                }
                $pathstring .= $folder;
            }
            $file = $this->site['path']['content'] . $pathstring . '.php';
            $attributes['href'] = $file;

            if (!array_key_exists('title', $attributes)) {
                $attributes['title'] = $data[1];
            }
            if ($text === '') {
                $text = $data[0];
            }
            return $this->pageBuilder->getTag('a', TagType::Full, $attributes, $text);
        }
        return $text;
    }

    /**
     * getter for all includes to add to the pages head
     * 
     * @return string the DOM
     */
    public function getHeadIncludes()
    {
        $result = "";
        foreach ($this->features as $id => $feature) {
            $result .= $this->getFeature($id)->getHeadIncludes();
        }
        return $result;
    }

    private function getStructureItemPath($id)
    {
        return $this->getSubStructureItemPath($this->site['structure'], $id);
    }

    private function getSubStructureItemPath($haystack, $needle)
    {
        $path = array();
        $result = array();
        foreach ($haystack as $id => $data) {
            if ($id === $needle) {
                $result[] = $needle;
                return $result;
            }
            if (array_key_exists(2, $data)) {
                $subPath = $this->getSubStructureItemPath($data[2], $needle);
                if ($subPath !== null) {
                    $path[] = $id;
                    return array_merge($path, $subPath);
                }
            }
        }
        return null;
    }

    private function getSiteStructureItemData($id)
    {
        return $this->getSubStructureItemData($this->site['structure'], $id);
    }

    private function getSubStructureItemData($structure, $needle)
    {
        foreach ($structure as $id => $data) {
            if ($id === $needle) {
                return $data;
            }
            if (array_key_exists(2, $data)) {
                $prospectSubData = $this->getSubStructureItemData($data[2], $needle);
                if ($prospectSubData !== null) {
                    return $prospectSubData;
                }
            }
        }
        return null;
    }

    // feature-handling
    private function getActiveFeatures()
    {
        $features = array();
        foreach ($this->featureconfig as $id => $config) {
            if (isset($config['active']) && $config['active']) {
                $features[$id] = $this->initFeature($id);
            }
        }
        return $features;
    }

    private function initFeature($id)
    {
        $config = $this->featureconfig[$id];
        $clazz = "De\\Macbarfuss\\SimpleFramework\\Feature\\" . $id;
        return new $clazz($config);
    }

    /**
     * getter to all configured features
     * 
     * @param string $id id of the feature
     * @return unknown instance of the feature
     */
    public function getFeature($id)
    {
        return $this->features[$id];
    }

    // exposing tools
    /**
     * getter to the NavBuilder instance
     * 
     * @return unknown the instance
     */
    public function getNavBuilder()
    {
        return ClassRegistry::getInstance("NavBuilder");
    }

    /**
     * getter to the PageBuilder instance
     * 
     * @return unknown the instance
     */
    public function getPageBuilder()
    {
        return ClassRegistry::getInstance("PageBuilder");
    }

    /**
     * getter to the QuotationMarkBuilder instance
     * 
     * @return unknown the instance
     */
    public function getQuotationMarkBuilder()
    {
        return ClassRegistry::getInstance("QuotationMarkBuilder");
    }

    /**
     * getter to the SnippetHandler instance
     * 
     * @return unknown the instance
     */
    public function getSnippetHandler()
    {
        return ClassRegistry::getInstance("SnippetHandler");
    }
}
