<?php
namespace De\Macbarfuss\SimpleFramework\Feature;

use De\Macbarfuss\SimpleFramework\ClassRegistry as ClassRegistry;
use De\Macbarfuss\SimpleFramework\Feature as Feature;
use De\Macbarfuss\SimpleFramework\Manager as Manager;
use De\Macbarfuss\SimpleFramework\TagType as TagType;

/**
 * providing an image slideshow
 * 
 * set this variable in your site-specific config file to enable this feature:
 * $features['Slideshow']['active'] = true;
 */
class Slideshow extends Feature
{

    var $config;

    var $pageBuilder;

    /**
     * @inheritdoc
     */
    public function __construct($config = array())
    {
        $this->config = $config;
        $this->pageBuilder = ClassRegistry::getInstance("PageBuilder");
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return "Slideshow";
    }

    /**
     * @inheritdoc
     */
    public function isActiveAndValid()
    {
        if (array_key_exists('active', $this->config)) {
            $active = $this->config['active'];
            return $active;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function getDependencies()
    {
        $result = array();
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getHeadIncludes()
    {
        $result = '';

        // including the external library
        $result .= $this->pageBuilder->getTag('script', TagType::Full, array(
            'type' => 'text/javascript',
            'src' => 'http://slideshow.triptracker.net/slide.js'
        ));
        $result .= $this->pageBuilder->getTag('script', TagType::Open);

        // create the viewer instance
        $result .= 'var viewer = new PhotoViewer();';

        // add all files
        foreach ($this->config['images'] as $photo) {
            $result .= 'viewer.add("' . $this->config['basePath'] . $this->config['galleryPath'] . $photo . '");';
        }

        // set options
        $result .= $this->setBooleanOption('AutoPlay');
        $result .= $this->setStringOption('BackgroundColor');
        $result .= $this->setNumberOption('BorderWidth');
        $result .= $this->setBooleanOption('EmailLink');
        $result .= $this->setBooleanOption('Fading');
        $result .= $this->setStringOption('Font');
        $result .= $this->setNumberOption('FontSize');
        $result .= $this->setBooleanOption('Loop');
        if (array_key_exists('OnClickEvent', $this->config)) {
            $result .= 'viewer.setOnClickEvent(viewer.' . $this->config['OnClickEvent'] . ');';
        }
        $result .= $this->setBooleanOption('Panning');
        $result .= $this->setBooleanOption('PhotoFading');
        $result .= $this->setBooleanOption('PhotoLink');
        $result .= $this->setBooleanOption('Shade');
        $result .= $this->setStringOption('ShadeColor');
        $result .= $this->setNumberOption('ShadeOpacity');
        $result .= $this->setNumberOption('SlideDuration');
        $result .= $this->setBooleanOption('Toolbar');
        $result .= $this->setBooleanOption('ToolbarAnimator');

        $result .= $this->pageBuilder->getTag('script', TagType::Close);
        return $result;
    }

    /**
     * A boolean option must be set to 'enable' for true and 'disable' for false value.
     *
     * @param string $option
     * @return string
     */
    private function setBooleanOption($option)
    {
        if (array_key_exists($option, $this->config)) {
            return 'viewer.' . $this->config[$option] . $option . '();';
        }
    }

    private function setNumberOption($option)
    {
        if (array_key_exists($option, $this->config)) {
            return 'viewer.set' . $option . '(' . $this->config[$option] . ');';
        }
    }

    private function setStringOption($option)
    {
        if (array_key_exists($option, $this->config)) {
            return 'viewer.set' . $option . "('" . $this->config[$option] . "');";
        }
    }

    /**
     * @inheritdoc
     */
    public function getUi($linkcontent = 'Slideshow')
    {
        $result = '';
        if (array_key_exists('thumbnails', $this->config) && $this->config['thumbnails'] === true) {
            // open the area
            $result .= $this->pageBuilder->getTag('div', TagType::Open, array(
                'class' => 'slideshow'
            ));

            // for each image make a thumbnail
            $imgCount = 0;
            foreach ($this->config['images'] as $photo) {
                $imgPath = $this->config['basePath'] . $this->config['galleryPath'] . $photo;
                $thumbPath = $this->config['basePath'] . $this->config['galleryPath'] . 'thumbs/' . $photo;

                $result .= $this->pageBuilder->getTag('a', TagType::Open, array(
                    'href' => $imgPath,
                    'onclick' => 'return viewer.show(' . $imgCount . ')'
                ));
                $result .= $this->pageBuilder->getTag('img', TagType::NoContent, array(
                    'class' => 'thumb',
                    'src' => $thumbPath,
                    'alt' => $photo
                ));
                $result .= $this->pageBuilder->getTag('a', TagType::Close);

                $imgCount ++;

                // $result .= 'viewer.add("' . $this->config['basePath'] . $this->config['galleryPath'] . $photo . '");';
            }
            // <a href="photo/1.jpg" onclick="return viewer.show(0)">
            // <img width="70" height="70" src="photo/thumb_1.jpg" alt="" /> </a>
            // <a href="photo/2.jpg" onclick="return viewer.show(1)">
            // <img width="70" height="70" src="photo/thumb_2.jpg" alt="" /> </a>
            // <a href="photo/3.jpg" onclick="return viewer.show(2)">
            // <img width="70" height="70" src="photo/thumb_3.jpg" alt="" /> </a>
            // <a href="photo/4.jpg" onclick="return viewer.show(3)">
            // <img width="70" height="70" src="photo/thumb_4.jpg" alt="" /> </a>
            // <a href="photo/5.jpg" onclick="return viewer.show(4)">
            // <img width="70" height="70" src="photo/thumb_5.jpg" alt="" /> </a>
            $result .= $this->pageBuilder->getTag('div', TagType::Close);
        } else {
            $result .= '<a href="javascript:void(viewer.show(0))">' . $linkcontent . '</a>';
        }

        return $result;
    }
}
