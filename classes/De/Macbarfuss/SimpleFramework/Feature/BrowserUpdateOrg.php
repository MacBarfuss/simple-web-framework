<?php
namespace De\Macbarfuss\SimpleFramework\Feature;

use De\Macbarfuss\SimpleFramework\Feature as Feature;

/**
 * display the browser-update.org header
 * 
 * set this variable in your site-specific config file to enable this feature:
 * $features['BrowserUpdateOrg']['active'] = true;
 *
 */
class BrowserUpdateOrg extends Feature
{

    var $config;

    /**
     * @inheritdoc
     */
    public function __construct($config = array())
    {
        $this->config = $config;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return "BrowserUpdateOrg";
    }

    /**
     * @inheritdoc
     */
    public function isActiveAndValid()
    {
        if (array_key_exists('active', $this->config)) {
            $active = $this->config['active'];
            return $active;
        }
        return false;
    }
    
    /**
     * @inheritdoc
     */
    public function getDependencies()
    {
        $result = array();
        return $result;
    }
    
    /**
     * @inheritdoc
     */
    public function getHeadIncludes()
    {
        $result = '';
        $result .= '<script>';
        $result .= file_get_contents($this->getFsLibPath() . 'include.js');
        $result .= '</script>';
        return $result;
    }
}
