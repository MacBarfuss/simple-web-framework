<?php
namespace De\Macbarfuss\SimpleFramework\Feature;

use De\Macbarfuss\SimpleFramework\Feature as Feature;
use De\Macbarfuss\SimpleFramework\ClassRegistry;

/**
 * The SocialSharePrivacy Module relased by heise.de.
 * http://www.heise.de/extras/socialshareprivacy/
 * Version 1.6 is implemented here.
 * 
 * set this variable in your site-specific config file to enable this feature:
 * $features['SocialSharePrivacy']['active'] = true;
 */
class SocialSharePrivacy extends Feature
{

    var $config;

    /**
     * @inheritdoc
     */
    public function __construct($config = array())
    {
        $this->config = $config;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return "SocialSharePrivacy";
    }

    /**
     * @inheritdoc
     */
    public function isActiveAndValid()
    {
        if (array_key_exists('active', $this->config)) {
            $active = $this->config['active'];
            return $active;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function getDependencies()
    {
        $result = array();
        // $result['JQuery'] = '1.7.0 -';
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getUi()
    {
        return '<div id="socialshareprivacy"></div>';
    }

    /**
     * @inheritdoc
     */
    public function getHeadIncludes()
    {
        $fsRoot = $this->getFsLibPath();
        $webRoot = $this->getWebLibPath();
        $result = "";
        $result .= '<script type="text/javascript" src="' . $webRoot . 'jquery.socialshareprivacy.min.js"></script>';
        $result .= '<script type="text/javascript">';
        $result .= 'jQuery(document).ready(function($){';
        $result .= "    $('#socialshareprivacy').socialSharePrivacy(";
        $result .= $this->getOptionsJson();
        $result .= '    );';
        $result .= '});';
        $result .= '</script>';
        return $result;
    }

    private function getOptionsJson()
    {
        $webRoot = $this->getWebLibPath();
        $options = array();
        $options['css_path'] = $webRoot . "socialshareprivacy/socialshareprivacy.css";
        $options['lang_path'] = $webRoot . 'socialshareprivacy/lang/';
        $options['language'] = "de";
        $options['perma_option'] = "on";
        $options['perma_orientation'] = "top";
        $options['services']['gplus'] = $this->getGPlusOptions();
        $options['services']['facebook'] = $this->getFacebookOptions();
        $options['services']['twitter'] = $this->getTwitterOptions();
        $options['skin'] = "light";
        if (array_key_exists('uri', $this->config)) {
            $options['uri'] = $this->config['uri'];
        }
        return json_encode($options, JSON_UNESCAPED_SLASHES);
    }

    private function getFacebookOptions()
    {
        $webRoot = $this->getWebLibPath();
        $options = array();
        $options["action"] = "like";
        $options["dummy_img"] = $webRoot . "socialshareprivacy/images/dummy_facebook.png";
        $options['perma_option'] = "on";
        $options["status"] = "on";
        return $options;
    }

    private function getTwitterOptions()
    {
        $webRoot = $this->getWebLibPath();
        $options = array();
        $options["dummy_img"] = $webRoot . "socialshareprivacy/images/dummy_twitter.png";
        $options['perma_option'] = "on";
        $options["status"] = "on";
        return $options;
    }

    private function getGPlusOptions()
    {
        $options = array();
        $options["status"] = "off";
        return $options;
    }
}
