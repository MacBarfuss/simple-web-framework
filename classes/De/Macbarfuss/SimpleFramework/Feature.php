<?php
namespace De\Macbarfuss\SimpleFramework;

use De\Macbarfuss\SimpleFramework\ClassRegistry as ClassRegistry;

/**
 * abstract class to define hooks and structure of features
 * 
 */
abstract class Feature
{

    /**
     * get the path the external libraries are stored in
     * 
     * @return string the absolute path to the folder with trailing slash
     */
    public function getFsLibPath()
    {
        $result = '';
        $manager = ClassRegistry::getInstance("Manager");
        $result .= $manager->getSiteConfig()['fsroot'];
        $result .= 'lib/';
        $result .= $this->getId() . '/';
        return $result;
    }

    /**
     * get the path html templates are stored in
     * 
     * @return string the absolute path to the folder with trailing slash
     */
    public function getWebLibPath()
    {
        $result = '';
        $manager = ClassRegistry::getInstance("Manager");
        $result .= $manager->getSiteConfig()['path']['framework'];
        $result .= 'lib/';
        $result .= $this->getId() . '/';
        return $result;
    }

    /**
     * default constructor
     * 
     * @param array $config the config array for this feature
     */
    abstract public function __construct($config = array());

    /**
     * getter to identify the feature
     * 
     * this is used to find the config in the global features-config
     * 
     * @return string an unique identifier among all available features
     */
    abstract public function getId();

    /**
     * check to see if the feature is activated and the configuration does not have errors
     * 
     * @return boolean true if the feature is activated and the configuation is valid
     */
    abstract public function isActiveAndValid();

    /**
     * getter to the DOM to load to a pages head to be able to use the feature in a page
     * 
     * @return string the DOM
     */
    abstract public function getHeadIncludes();
}
