<?php
namespace De\Macbarfuss\SimpleFramework\Builder;

/**
 * handling snippets for easy loading.
 */
class SnippetHandler
{

    private $site;

    private $snippetdir;

    /**
     * default contructor
     * 
     * @param array $site configuration from global site config
     */
    public function __construct($site = array('structure' => array(), 'root' => ''))
    {
        $this->site = $site;
        if (array_key_exists('path', $site)) {
            $this->snippetdir = $this->site['path']['snippets'];
        }
    }

    /**
     * getting the full path to a snippet
     * 
     * @param string $id the id of the snippet
     * @return string the absolute path to the snippet
     */
    public function getSnippetPath($id)
    {
        return $this->snippetdir . $id . '.php';
    }
}
