<?php

namespace De\Macbarfuss\SimpleFramework\Builder;

/**
 * A builder to surround a text with correctly typesetted quotation marks.
 */
class QuotationMarkBuilder
{

    private $style;

    /**
     * Default constructor
     *
     * @param array $style
     *            style configuration from global page config.
     */
    public function __construct($style = array('language' => 'de'))
    {
        $this->style = $style;
    }

    /**
     * surround a text with double quotes
     *
     * @param string $text
     *            the text to be surrounded
     * @return string the surrounded text
     */
    public function doubleQuoted($text = "")
    {
        return "&bdquo;" . $text . "&ldquo;";
    }
}
