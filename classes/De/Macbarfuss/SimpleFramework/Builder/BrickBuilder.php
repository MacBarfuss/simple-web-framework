<?php
namespace De\Macbarfuss\SimpleFramework\Builder;

use De\Macbarfuss\SimpleFramework\PageBuilder;
use De\Macbarfuss\SimpleFramework\TagType as TagType;
use De\Macbarfuss\SimpleFramework\ClassRegistry;

/**
 * builder for adding small pieces of content in a specific layout
 *
 */
class BrickBuilder
{

    private $pageBuilder;

    private $site;

    private $style;

    /**
     * default constructor

     * @param array $site site-configuration from global config
     * @param array $style style-configuration from global config
     */
    public function __construct($site = array(), $style = array())
    {
        $this->pageBuilder = ClassRegistry::getInstance("PageBuilder");
        $this->site = $site;
        $this->style = $style;
    }
}
