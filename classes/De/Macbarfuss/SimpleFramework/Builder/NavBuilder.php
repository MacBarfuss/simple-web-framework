<?php
namespace De\Macbarfuss\SimpleFramework\Builder;

use De\Macbarfuss\SimpleFramework\PageBuilder;
use De\Macbarfuss\SimpleFramework\TagType as TagType;
use De\Macbarfuss\SimpleFramework\ClassRegistry;

/**
 * builder for creating a navigation DOM
 */
class NavBuilder
{

    private $pageBuilder;

    private $site;

    private $style;

    /**
     * default constructor
     *
     * @param array $site
     *            site-configuration from global site-config
     * @param array $style
     *            style-configuration from global site-config
     */
    public function __construct($site = array('structure' => array(), 'root' => ''), $style = array())
    {
        $this->pageBuilder = ClassRegistry::getInstance("PageBuilder");
        $this->site = $site;
        $this->style = $style;
    }

    /**
     * get the main nav DOM
     *
     * @param string $currentPage
     *            the pageid from the current page
     * @return string html dom of the main nav
     */
    public function getMainNav($currentPage = "")
    {
        $navclass = $this->getMainNavStyle();
        if (isset($this->style['autopath'])) {
            $autopath = $this->style['autopath'];
        } else {
            $autopath = true;
        }
        $result = "";
        $result .= $this->pageBuilder->getTag('ul', TagType::OPEN, array(
            'class' => $navclass
        ));

        $imgLinkRoot = null;
        if ($this->useImageLinks()) {
            if (isset($this->style['imageLinks']['path'])) {
                $imgLinkRoot = $this->site['path']['root'] . $this->style['imageLinks']['path'];
            } else {
                $imgLinkRoot = $this->site['path']['root'] . 'images/menu/';
            }
        }

        $firstItem = true;
        foreach ($this->site['structure'] as $id => $data) {
            $text = $data[0];
            $title = $data[1];
            if ($firstItem) {
                $file = $this->site['path']['root'] . 'index.php';
                $firstItem = false;
            } elseif ($autopath) {
                $file = $this->site['path']['content'] . $id . '/';
            } else {
                $file = $this->site['path']['root'] . $data[3];
            }
            $result .= $this->getNavItem($file, $id, $currentPage, $text, $title, $imgLinkRoot);
        }
        $result .= $this->pageBuilder->getTag('ul', TagType::CLOSE);
        return $result;
    }

    private function getNavItem($target, $pageId, $currentPage, $text, $title = null, $imgroot = null)
    {
        $result = "";
        $liClass = array(
            'nav-item'
        );
        if ($pageId === $currentPage) {
            $liClass[] = 'active';
        }
        $liAttributes = array(
            'class' => implode(' ', $liClass)
        );
        $result .= $this->pageBuilder->getTag('li', TagType::OPEN, $liAttributes);

        $linkAttributes = array(
            'href' => $target,
            'class' => 'nav-link'
        );
        if ($title !== null) {
            $linkAttributes['title'] = $title;
        }
        $result .= $this->pageBuilder->getTag('a', TagType::OPEN, $linkAttributes);
        if ($imgroot !== null) {
            $imgPath = $imgroot;
            if ($pageId === $currentPage) {
                $imgPath .= 'active';
            } else {
                $imgPath .= 'default';
            }
            $imgPath .= '/' . $pageId . '.png';
            $result .= $this->pageBuilder->getTag('img', TagType::NO_CONTENT, array(
                'src' => $imgPath,
                'alt' => $text
            ));
        } else {
            $result .= $text;
        }
        $result .= $this->pageBuilder->getTag('a', TagType::CLOSE);
        $result .= $this->pageBuilder->getTag('li', TagType::CLOSE);
        return $result;
    }

    /*
     * Returns whether the Image-Link feature for Navbar should be used.
     */
    private function useImageLinks()
    {
        if (isset($this->style['imageLinks'])) {
            $imageLinksFeature = $this->style['imageLinks'];
            if (isset($imageLinksFeature['active'])) {
                return $imageLinksFeature['active'];
            }
        }
        return false;
    }

    /**
     * get a subnav DOM
     *
     * @param string $currentPage
     *            id of the current page to get the subnav for
     * @return string the subnav dom
     */
    public function getSubNav($currentPage)
    {
        if (isset($this->style['sub_class'])) {
            $navclass = $this->style['sub_class'];
        } else {
            $navclass = 'nav menu';
        }
        if (isset($this->style['autopath'])) {
            // TODO make autopath default
            $autopath = $this->style['autopath'];
        } else {
            $autopath = false;
        }
        $result = "";
        if (isset($this->site['structure'][$currentPage[0]][2])) {
            $items = $this->site['structure'][$currentPage[0]][2];
            $opening = $this->pageBuilder->getTag('ul', TagType::OPEN, array(
                'class' => $navclass
            ));
            $result .= $opening;
            foreach ($items as $id => $data) {
                if ($this->isDelimiter($id)) {
                    $result .= $this->pageBuilder->getTag('ul', TagType::CLOSE);
                    $attributes = array();
                    $attributes['class'] = 'delimiter';
                    if (isset($data[2])) {
                        $attributes['title'] = $data[2];
                    }
                    $result .= $this->pageBuilder->getTag('div', TagType::OPEN, $attributes);
                    $result .= $data[1];
                    $result .= $this->pageBuilder->getTag('div', TagType::CLOSE);
                    $result .= $opening;
                } else {
                    $text = $data[0];
                    $title = $data[1];
                    if ($autopath) {
                        if ($id === $currentPage[0]) {
                            $file = $this->site['path']['content'] . $currentPage[0] . '/';
                        } else {
                            $file = $this->site['path']['content'] . $currentPage[0] . '/' . $id . '.php';
                        }
                    } else {
                        $file = $this->site['path']['root'] . $data[2];
                    }
                    if (isset($currentPage[1])) {
                        $result .= $this->getSubnavItem($file, $id, $currentPage[1], $text, $title);
                    } else {
                        $result .= $this->getSubnavItem($file, $id, $currentPage[0], $text, $title);
                    }
                }
            }
            $result .= $this->pageBuilder->getTag('ul', TagType::CLOSE);
        }
        return $result;
    }

    private function getSubnavItem($target, $pageId, $currentPage, $text, $title = null)
    {
        $attributes = array(
            'href' => $target,
            'class' => 'nav-link'
        );
        if ($title !== null) {
            $attributes['title'] = $title;
        }
        $result = "";
        if ($pageId === $currentPage) {
            $result .= $this->pageBuilder->getTag('li', TagType::OPEN, array(
                'class' => 'nav-item active'
            ));
        } else {
            $result .= $this->pageBuilder->getTag('li', TagType::OPEN, array(
                'class' => 'nav-item'
            ));
        }
        $result .= $this->pageBuilder->getTag('a', TagType::FULL, $attributes, $text);
        $result .= $this->pageBuilder->getTag('li', TagType::CLOSE);
        return $result;
    }

    // HELPERS
    /**
     * getter to read the main nav style from config
     *
     * @return string the css class string to style main nav
     */
    public function getMainNavStyle()
    {
        if (isset($this->style['main_class'])) {
            return $this->style['main_class'];
        } else {
            return 'nav';
        }
    }

    /**
     * check to tell if a nav item is a delimeter
     *
     * @param string $id
     *            to check
     * @return boolean whether the nac item is a delimiter or not
     */
    public function isDelimiter($id)
    {
        return (substr($id, 0, 1) === '#');
    }
}
