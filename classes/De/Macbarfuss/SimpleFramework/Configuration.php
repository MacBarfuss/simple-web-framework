<?php
namespace De\Macbarfuss\SimpleFramework;

/**
 * factory and wrapper for configuration items
 */
class Configuration
{

    public $paths;

    public $pageId;

    /**
     * default constructor
     */
    public function __construct()
    {
        $this->paths = array(
            'root' => '../../'
        );
    }

    public function getPageId($level = 0)
    {
        if ($level === 0) {
            return $this->pageId;
        } else {
            return $this->pageId[$level - 1];
        }
    }

    public function setPageId($pageId)
    {
        $this->pageId = $pageId;
    }

    public function setPageIdFromURI($uri)
    {
        $uri_parts = explode('/', $uri);
        $pageId = array_slice($uri_parts, -2);
        $pageId[1] = str_replace(".php", "", $pageId[1]);
        if ($pageId[1] == "") {
            $pageId[1] = $pageId[0];
        }

        $this->pageId = $pageId;
    }

    public function getPath($key)
    {
        return $this->paths[$key];
    }

    public function setPath($key, $value)
    {
        $this->paths[$key] = $value;
    }
}
