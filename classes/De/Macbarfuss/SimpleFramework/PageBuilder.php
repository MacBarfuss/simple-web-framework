<?php
namespace De\Macbarfuss\SimpleFramework;

use De\Macbarfuss\SimpleFramework\TagType as TagType;

/**
 * builder to add things to a page
 */
class PageBuilder
{

    /**
     * getter for a blind text for development purpose
     *
     * @return string two medium sized paragraphs of lorem ipsum
     */
    public function getBlindtext()
    {
        return "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum
                    mauris. Maecenas congue ligula ac quam viverra nec consectetur ante hendrerit. Donec et mollis
                    dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim,
                    ut porta lorem lacinia consectetur. Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem
                    ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut gravida lorem. Ut turpis felis,
                    pulvinar a semper sed, adipiscing id dolor.</p>
                <p>Pellentesque auctor nisi id magna consequat sagittis. Curabitur dapibus enim sit amet elit pharetra
                    tincidunt feugiat nisl imperdiet. Ut convallis libero in urna ultrices accumsan. Donec sed odio
                    eros. Donec viverra mi quis quam pulvinar at malesuada arcu rhoncus. Cum sociis natoque penatibus et
                    magnis dis parturient montes, nascetur ridiculus mus. In rutrum accumsan ultricies. Mauris vitae
                    nisi at sem facilisis semper ac in est.</p>";
    }

    /**
     * getter to create a new HTML tag
     *
     * @param string $tag
     *            name of the tag
     * @param TagType $type
     *            one of OPEN, CLOSE, FULL or NO_CONTENT
     * @param array $attributes
     *            named array to add attributes (not useful for CLOSE-tags)
     * @param string $content
     *            what to fill after the opening tag (so not useful for CLOSE- and NO_CONTENT-tags
     * @return string the DOM
     */
    function getTag($tag, $type, $attributes = array(), $content = "")
    {
        $result = "";
        if ($type !== TagType::CLOSE) {
            $result .= "<" . $tag;
            foreach ($attributes as $key => $value) {
                $result .= " " . $key . "=\"" . $value . "\"";
            }
            if ($type === TagType::NO_CONTENT) {
                $result .= "/";
                $content = "";
            }
            $result .= ">";
        }
        $result .= $content;
        if ($type === TagType::CLOSE || $type === TagType::FULL) {
            $result .= "</" . $tag . ">";
        }
        return $result;
    }

    // shortcuts
    // =========================================================================
    /**
     * shortcut for creating a <div> element
     *
     * @param string $id
     *            text for the id attribute
     * @param TagType $type
     *            one of OPEN, CLOSE, FULL
     * @param string $content
     *            content for the div
     * @return string the DOM
     */
    function getDiv($id, $type, $content = "")
    {
        return $this->getTag('div', $type, array(
            'id' => $id
        ), $content);
    }
}
