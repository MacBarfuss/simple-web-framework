<?php
namespace De\Macbarfuss\SimpleFramework;

/**
 * static container to hold instances of other clcasses
 */
abstract class ClassRegistry
{

    /**
     * storage for shared objects
     *
     * @var array
     */
    private static $instances = array();

    /**
     * getter to retriev an instance of one specific class
     * 
     * this tries to create a new instance if there is non for a class name
     * 
     * @param string $className to look for
     * @return unknown the instance
     */
    static public function getInstance($className)
    {
        if (!isset(self::$instances[$className])) {
            self::$instances[$className] = new $className();
        }
        return self::$instances[$className];
    }

    /**
     * adding an instance created outside of the registry
     * 
     * @param string $name the class name of the instance to be added
     * @param unknown $instance the instance to be added
     */
    static public function addInstance($name, $instance)
    {
        self::$instances[$name] = $instance;
    }
}
