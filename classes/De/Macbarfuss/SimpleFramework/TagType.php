<?php
namespace De\Macbarfuss\SimpleFramework;

/**
 * enum to control generation of tags
 *
 */
abstract class TagType
{

    const OPEN = 0;

    const CLOSE = 1;

    const NO_CONTENT = 2;

    const FULL = 3;
}
