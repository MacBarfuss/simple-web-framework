# Changelog

Use this file to find out what to do to be compatible with the next version.

Changes which are not compatible with a prior version is marked with 'Compatibility warning!', followed by additional information. Patch releases always keep downward compatibility.

## v0.4

### New features

* the main nav is created without the wrapping nav. The template must add this itself so this is useable in a navbar to. Compatibility warning! Check your navigation!
* configuration can now be placed in the Configuration wrapper.

## v0.3

### New features

* subnavigation can be delimited by a special structure-entry.
* removed internal dependency-handling. use composer instead. Compatibility warning! All references to the dependency management must be removed.
* New Builder: QuotationMarkBuilder, can add quotation marks around a text.

### Bugfixes

* subnavigation uses bootstrap classes.

## v0.2

* dependencies take embedded libraries

## v0.1.1

* there were wrong references on `$site['root]` in NavBuilder, which were changed to `$site['path']['root']`;
* imageLinks did not have a default path as documented.

## v0.1

* `$pageId` must be an array with two items if sub-menu is used.
* `$site['structure']` now has a new field for the title attribute which must exist for each item.

## v0.0.5

* new parameter 'uri' for SocialSharePrivacy.

## v0.0.4

* new class NavBuilder has to be asked for navigation snippets. Manager does not longer have this methods.
* the config items for image links in the main navigation has been moved to style->nav.
* Dependencies are now set as "Bootstrap" and "JQuery", not the former used lowercase-version.

## v0.0.3

* the site-structure variable must no longer contain the pointer to root but only the relative path assumed from root. (#12)
* new function to get an internal link. (#13)
* add the headInclude-UpdateHook to any page.

## v0.0.2
**Autoloading**

* the file ```include.php``` was removed and now ```init.php``` has to be included.

## v0.0.1
**first release after initial creation beside of relilema.de**

### Navigation style configuration

To set a style class for main navigation set following variable in ```config.php```:

	$style['nav'] = 'nav nav-tabs';

This is optional and defaults to ```nav menu```.

### ImageLink as configurable feature

Using images as links in a menu can now be switched off and the path to the images can be configured.
Add following lines to ```config.php``` for this feature:

	$features['nav']['imageLinks']['active'] = true;
	$features['nav']['imageLinks']['path'] = 'images/menu/';

If the feature is activated the path must be set. It can be an empty string to load images from root path. To not use
the feature just set the active-flag to false or leave it unset.
