<?php
use De\Macbarfuss\SimpleFramework\Manager as Manager;
use PHPUnit\Framework\TestCase;

/**
 * Tests for the Manager class.
 */
class ManagerTest extends TestCase
{
    public $classUnderTest;

    /**
     * @before
     */
    public function setUp(): void
    {
        $this->classUnderTest = new Manager(array(), array());
    }

    /**
     * Basic instatiation tests.
     */
    public function testInstatiation()
    {
        $this->assertNotNull($this->classUnderTest);
    }
}
