<?php
use De\Macbarfuss\SimpleFramework\Configuration as Configuration;
use PHPUnit\Framework\TestCase;

/**
 * Tests for the Configuration class.
 */
class ConfigurationTest extends TestCase
{
    public $classUnderTest;

    /**
     * @before
     */
    public function setUp(): void
    {
        $this->classUnderTest = new Configuration();
    }

    /**
     * Basic instatiation tests.
     */
    public function testInstatiation()
    {
        $this->assertNotNull($this->classUnderTest);
    }
    
    /**
     * path root is prefilled
     */
    public function testPathRootIsPrefilled()
    {
        $this->assertEquals('../../', $this->classUnderTest->getPath('root'));
    }
    
    /**
     * set root path
     */
    public function testSetRootPath()
    {
        $this->assertEquals('../../', $this->classUnderTest->getPath('root'));
        $this->classUnderTest->setPath('root', '');
        $this->assertEquals('', $this->classUnderTest->getPath('root'));
    }
}
