<?php
use PHPUnit\Framework\TestCase;

use De\Macbarfuss\SimpleFramework\Builder\NavBuilder;
use De\Macbarfuss\SimpleFramework\PageBuilder;
use De\Macbarfuss\SimpleFramework\ClassRegistry;

/**
 * Tests for the NavBuilder-Class.
 */
class NavBuilderTest extends TestCase
{

    public $classUnderTest;

    /**
     *
     * @before
     */
    public function setUp(): void
    {
        ClassRegistry::addInstance('PageBuilder', new PageBuilder());
    }

    // =========================================================================
    // TESTS: default constructor
    // =========================================================================
    /**
     * Test if the class instatiation with empty parameters works.
     */
    public function testInstatiation()
    {
        $this->classUnderTest = new NavBuilder();
        $this->assertNotNull($this->classUnderTest);
    }

    // =========================================================================
    // TESTS: getMainNav($currentPage = "")
    // =========================================================================
    /**
     * Test for navigation with image links and default path behaviour.
     */
    public function testGetMainNavImageLinksWithoutPathHasDefaultPath()
    {
        // GIVEN just one site
        $site = array(
            'structure' => array(
                'index' => array(
                    'A',
                    'B'
                ),
                'foo' => array(
                    'bar',
                    'Foo Bar'
                )
            ),
            'path' => array(
                'content' => 'contentpath',
                'root' => ''
            )
        );
        // and we want to use imageLinks but don't provide a path
        $style = array();
        $style['imageLinks']['active'] = true;
        $style['autopath'] = true;

        // WHEN
        $this->classUnderTest = new NavBuilder($site, $style);
        $prospect = $this->classUnderTest->getMainNav('foo');

        // THEN the generated code has a correct link path
        $this->assertGreaterThan(0, strpos($prospect, 'images/menu/'));
    }

    /**
     * Test for small default navigation
     */
    public function testGetMainNavSmallDefault()
    {
        // GIVEN just a small site
        $site = array(
            'structure' => array(
                'index' => array(
                    'A',
                    'B'
                ),
                'foo' => array(
                    'bar',
                    'Foo Bar'
                )
            ),
            'path' => array(
                'content' => 'contentpath',
                'root' => ''
            )
        );

        // WHEN
        $this->classUnderTest = new NavBuilder($site);
        $prospect = $this->classUnderTest->getMainNav('foo');

        // THEN the generated code looks like expected
        $this->assertEquals('<ul class="nav"><li class="nav-item"><a href="index.php" class="nav-link" title="B">A</a></li><li class="nav-item active"><a href="contentpathfoo/" class="nav-link" title="Foo Bar">bar</a></li></ul>', $prospect);
    }

    // =========================================================================
    // TESTS: getMainNavStyle()
    // =========================================================================
    /**
     * Test for default style applied if not configured.
     */
    public function testGetMainNavStyleDefault()
    {
        // GIVEN no special config

        // WHEN
        $this->classUnderTest = new NavBuilder();
        $prospect = $this->classUnderTest->getMainNavStyle();

        // THEN a default is set
        $this->assertEquals('nav', $prospect);
    }

    /**
     * Test for configured style taking precedence over default.
     */
    public function testGetMainNavStyleFromConfig()
    {
        // GIVEN a style is configurated
        $style = array(
            'main_class' => 'my special style'
        );

        // and no special config for site
        $site = array();

        // WHEN
        $this->classUnderTest = new NavBuilder($site, $style);
        $prospect = $this->classUnderTest->getMainNavStyle();

        // THEN a default is set
        $this->assertEquals('my special style', $prospect);
    }

    // =========================================================================
    // TESTS: getSubNav($currentPage)
    // =========================================================================
    /**
     * Tests if an empty subnav array is okay and returns no subnav.
     */
    public function testGetSuNavCurrentPageHasNoSubnavResultIsEmpty()
    {
        // GIVEN structure without subpages
        $site = array(
            'structure' => array(
                'index' => array(
                    'A',
                    'B'
                ),
                'foo' => array(
                    'bar',
                    'Foo Bar'
                )
            ),
            'path' => array(
                'content' => 'contentpath',
                'root' => ''
            )
        );
        // and nothing special for style
        $style = array();

        // WHEN
        $this->classUnderTest = new NavBuilder($site, $style);
        $prospect = $this->classUnderTest->getSubNav(array(
            'foo'
        ));

        // THEN the generated code is an empty string
        $this->assertEquals('', $prospect);
    }

    /**
     * Tests if a formatted delimeter is rendered.
     */
    public function testGetSuNavDelimiterFormattedIsRendered()
    {
        // GIVEN structure with subpages and a formatted delimiter
        $site = array(
            'structure' => array(
                'index' => array(
                    'A',
                    'B'
                ),
                'foo' => array(
                    'bar',
                    'Foo Bar',
                    array(
                        'foo1' => array(
                            'Foo 1',
                            'What Foo 1'
                        ),
                        'foo2' => array(
                            'Foo 2',
                            'What Foo 2'
                        ),
                        '#delimiter' => array(
                            'newBox-formatted',
                            'Delimmi',
                            'The Foo Bar Delimiter'
                        ),
                        'bar1' => array(
                            'Bar 1',
                            'That Bar 1'
                        ),
                        'bar2' => array(
                            'Bar 2',
                            'That Bar 2'
                        )
                    )
                )
            ),
            'path' => array(
                'content' => 'contentpath',
                'root' => ''
            )
        );
        // and autopath active
        $style = array();
        $style['autopath'] = true;

        // WHEN
        $this->classUnderTest = new NavBuilder($site, $style);
        $prospect = $this->classUnderTest->getSubNav(array(
            'foo'
        ));

        // THEN the generated code contains the delimiter and the two blocks
        $expected = '';
        $expected .= '<ul class="nav menu">';
        $expected .= '<li class="nav-item"><a href="contentpathfoo/foo1.php" class="nav-link" title="What Foo 1">Foo 1</a></li>';
        $expected .= '<li class="nav-item"><a href="contentpathfoo/foo2.php" class="nav-link" title="What Foo 2">Foo 2</a></li>';
        $expected .= '</ul>';
        $expected .= '<div class="delimiter" title="The Foo Bar Delimiter">Delimmi</div>';
        $expected .= '<ul class="nav menu">';
        $expected .= '<li class="nav-item"><a href="contentpathfoo/bar1.php" class="nav-link" title="That Bar 1">Bar 1</a></li>';
        $expected .= '<li class="nav-item"><a href="contentpathfoo/bar2.php" class="nav-link" title="That Bar 2">Bar 2</a></li>';
        $expected .= '</ul>';
        $this->assertEquals($expected, $prospect);
    }
}
