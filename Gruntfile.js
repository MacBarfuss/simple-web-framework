module.exports = function(grunt) {
	grunt.initConfig({
//	pkg: grunt.file.readJSON('package.json')
		jshint : {
			files : [ 'Gruntfile.js', 'src/**/*.js', 'test/**/*.js' ],
			options : {
				globals : {
					jQuery : true
				}
			}
		},
		watch : {
			files : [ '<%= jshint.files %>' ],
			tasks : [ 'jshint' ]
		}
	});

	grunt.loadNpmTasks('grunt-contrib-copy');

	grunt.registerTask('default', [ 'jshint' ]);

};
