<?php

// autoload function
spl_autoload_register(function ($class) {
    // convert namespace to full file path
    $class = 'classes/' . str_replace('\\', '/', $class) . '.php';
    require_once ($class);
});

use De\Macbarfuss\SimpleFramework\ClassRegistry as ClassRegistry;
use De\Macbarfuss\SimpleFramework\Manager as Manager;

$site['fsroot'] = realpath(dirname(__FILE__)) . '/';

$manager = new Manager($site, $features, $style);
ClassRegistry::addInstance("Manager", $manager);

